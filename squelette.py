#! /usr/bin/env python3

import pygame
import sys

from Balle import Balle
#from math import random

global FPSCLOCK
FPS = 60
WINDOWWIDTH = 800
WINDOWHEIGHT = 600
ARRIERE_PLAN = (42,17,51)


class Quitte(Exception):
    pass

def isQuitEvent(event):
    return (event.type == pygame.QUIT or
            (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))

def handleKey(event):
    print("appui sur la touche", event.key)

def handleClick(event):
    print("Clic à la position", event.pos)

def handleEvents():
    for event in pygame.event.get():
        # pour chaque évènement depuis le dernier appel de cette fonction
        if isQuitEvent(event):
            raise Quitte
        elif event.type == pygame.KEYDOWN:
            handleKey(event)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            print("clic")

def refresh(s):
    s.fill(ARRIERE_PLAN)

def drawApp(s, t):
    """
    Redessine l'écran. 't' est le temps écoulé depuis l'image précédente.
    """
    x = int(0.09*t) % WINDOWWIDTH
    y = int(0.15*t) % WINDOWHEIGHT
    refresh(s)
    pygame.draw.circle(s, (0,255,100), (x,y), 30)

def main():
    temps = 0
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    pygame.display.set_caption('Balles colorées sur fond violet foncé')
    ecran = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    refresh(ecran)

    #pos,vit,cou,tai
    b1 =  Balle((0,0),(200,200),(0,255,255),16)
    b2 = Balle((20,0),(180,250),(255,0,255),16)

    balles = [b1,b2]

    temps_ecoule = 0

    while True:  #boucle principale
        try:
            handleEvents()
            # drawApp(ecran, temps)
            refresh(ecran)
            for balle in balles:
                balle.avance(temps_ecoule)
                balle.dessine(ecran)

            pygame.display.update()

            temps_ecoule = FPSCLOCK.tick(FPS)
            temps += temps_ecoule
        except Quitte:
            break


    pygame.quit()
    sys.exit(0)

main()
