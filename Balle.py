import pygame

class Balle:

    def __init__(self,pos,vit,cou,tai):
        self.position = pos
        self.vitesse = vit
        self.couleur = cou #(R,G,B)
        self.taille = tai

    def avance(self,t):
        px, py = self.position
        dx, dy = self.vitesse
        px = int(px + dx * t / 1000)
        py = int(py + dy * t / 1000)
        self.position = (px,py)

    def dessine(self,ecran):
        pygame.draw.circle(ecran, self.couleur, self.position, self.taille)

    def contient(self,position):
        #indique si la position passée en argument est située à l'intérieur de la balle
